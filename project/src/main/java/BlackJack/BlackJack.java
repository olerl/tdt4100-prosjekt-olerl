package BlackJack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class BlackJack {

	private List<Card> deck = new ArrayList<>();
	private Player player;
	private Dealer dealer;
	private String playerMessage;
	private List<String> playerHistory = new ArrayList<>();
	private String name;
	
	public BlackJack() {
		generateDeck(4);
		shuffleDeck();
		this.player = new Player();
		this.dealer = new Dealer();
		this.player.setGame(this);
		this.dealer.setGame(this);
	}
	
	public void generateDeck(int n) {
		if(n < 1) {
			throw new IllegalArgumentException("Number of decks to be generated must be larger than 0.");
		}
		char suits[] = "SHDC".toCharArray();
		if(getDeckSize() > 0) {
			int l = getDeckSize();
			for(int i=0; i<l; i++) {
				deck.remove(0);
			}
		}
		for (int i=0; i<n; i++) {
			for (int j=0; j<4; j++) {
				for (int k=0; k<13; k++) {
					this.deck.add(new Card(suits[j], k+1));
				}
			}
		}	
	}
	
	public void shuffleDeck() {
		List<Card> shuffled = new ArrayList<>();
		Random random = new Random();
		int l = getDeckSize();
		for(int i=0; i<l; i++) {
			int rand = random.nextInt(getDeckSize());
			shuffled.add(getCard(rand));
			deck.remove(rand);
		}
		deck = shuffled;
	}
	
	public Card getCard(int n) {
		return deck.get(n);
	}
	
	public Card deckPop() {
		Card card = getCard(0);
		deck.remove(0);
		return card;
	}
	
	public void dealHands() {
		if(getDeckSize() < 10) {
			generateDeck(4);
			shuffleDeck();
		}
		if(player.getCurrentHand() != null) {
			player.emptyHand();
		}
		player.addCardToHand(deckPop());
		player.addCardToHand(deckPop());
		
		if(dealer.getCurrentHand() != null) {
			dealer.emptyHand();
		}
		dealer.addCardToHand(deckPop());
		dealer.addCardToHand(deckPop());
	}
	
	public void handFinished() {
		if(!playerNotBust()) {
			player.lostHand();
		} else if (playerNotBust() && !dealerNotBust()) {
			player.wonHand();
		} else {
			if (player.getCurrentHand().evaluateHand() > dealer.getCurrentHand().evaluateHand()) {
				player.wonHand();
			} else if (player.getCurrentHand().evaluateHand() == dealer.getCurrentHand().evaluateHand()) {
				player.tieHand();
			} else {
				player.lostHand();
			}
		}
	}
	
	public List<Card> getDeck() {
		List<Card> copy = new ArrayList<>();
		for(int i=0; i<getDeckSize(); i++) {
			copy.add(getCard(i));
		}
		return copy;
	}
	public int getDeckSize() {
		return deck.size();
	}
	
	public boolean gameOver() {
		if(player.getChipCount() == 0) { playerMessage = "You are out of chips. Better luck next time!"; }
		return player.getChipCount() == 0;
	}
	
	public Hand getPlayerHand() {
		return player.getCurrentHand();
	}
	
	public Hand getDealerHand() {
		return dealer.getCurrentHand();
	}
	
	public void playerBet(String bet) {
		player.placeBet(bet);
	}
	
	public String getPlayerChipCount() {
		return String.valueOf(player.getChipCount());
	}
	
	public String getPlayerCurrentBet() {
		return String.valueOf(player.getCurrentBet());
	}
	
	public void playerHit() {
		player.addCardToHand(deckPop());
	}
	
	public int getPlayerHandSize() {
		return getPlayerHand().getSize();
	}
	
	public int getDealerHandSize() {
		return getDealerHand().getSize();
	}
	
	public boolean playerNotBust() {
		return getPlayerHand().notBust();
	}
	
	public boolean dealerNotBust() {
		return getDealerHand().notBust();
	}
	
	public void dealerPlay() {
		dealer.dealerPlay();
	}
	
	public void setPlayerMessage(String message) {
		playerMessage = message;
	}
	
	public String getPlayerMessage() {
		return playerMessage;
	}
	
	public void addToPlayerHistory(String result) {
		playerHistory.add(0, result);
	}
	
	public List<String> getPlayerHistory() {
		List<String> copy = new ArrayList<>();
		for(int i=0; i<playerHistory.size(); i++) {
			copy.add(playerHistory.get(i));
		}
		return copy;
	}
	
	public void setName(String s) {
		this.name = s;
	}
	
	public String getName() {
		return name;
	}
	
	public void setPlayerChipCount(int chipCount) {
		player.setChipCount(chipCount);
	}
}
