package BlackJack;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.Scanner;

public class BlackJackFileSupport implements BlackJackFileReadingInterface{

	@Override
	public BlackJack readBlackJack(InputStream is) {
		BlackJack blackJack = new BlackJack();
		try (var scanner = new Scanner(is)) {
			while (scanner.hasNextLine()) {
				var line = scanner.nextLine().stripTrailing();
				if (line.isEmpty() || line.startsWith("#")) {
					continue;
				}
				if (blackJack.getName() == null) {
					blackJack.setName(line);
				} else if (line.startsWith("[")) {
					line = line.substring(1, line.length()-1);
					String[] playerHistory = line.split(",");
					for (int i=playerHistory.length-1; i>=0; i--) {
						try {
							Integer.parseInt(playerHistory[i].substring(1));
							blackJack.addToPlayerHistory(playerHistory[i].strip());
						}
						catch (NumberFormatException nfe){
							return backupGame();
						}
					}
				} else {
					try {
						blackJack.setPlayerChipCount(Integer.parseInt(line));
						blackJack.setPlayerMessage("Game loaded successfully.");
					}
					catch (NumberFormatException nfe) {
						return backupGame();
					}
				}
				
			}
		}
		return blackJack;
	}
	
	public BlackJack backupGame() {
		BlackJack backupGame = new BlackJack();
		backupGame.setPlayerMessage("Corrupted file - game not loaded successfully.");
		return backupGame;
	}

	@Override
	public BlackJack readBlackJack(String name, BlackJack blackJack) {
		Path path = Path.of(name);
		try {
			var input = new FileInputStream(path.toFile());
			return readBlackJack(input);
		}
		catch (FileNotFoundException fnfe) {
			blackJack.setPlayerMessage("Unable to save due to file not found.");
			return blackJack;
		}
		catch (SecurityException se) {
			blackJack.setPlayerMessage("Unable to save due to security violation.");
			return blackJack;
		}
	}

	@Override
	public void writeBlackJack(BlackJack blackJack, OutputStream os) {
		try (var writer = new PrintWriter(os)) {
			writer.println("# name");
			writer.println(blackJack.getName());
			writer.println("# chip count");
			writer.println(blackJack.getPlayerChipCount());
			writer.println("# player history");
			writer.println(blackJack.getPlayerHistory());
		}
	}

	@Override
	public void writeBlackJack(BlackJack blackJack) {
		Path path = Path.of(blackJack.getName());
		try {
			var output = new FileOutputStream(path.toFile());
			writeBlackJack(blackJack, output);
			blackJack.setPlayerMessage("Game saved successfully.");
		}
		catch (FileNotFoundException fnfe) {
			blackJack.setPlayerMessage("Unable to save due to file not found.");
		}
		catch (SecurityException se) {
			blackJack.setPlayerMessage("Unable to save due to security violation.");
		}
	}

}
