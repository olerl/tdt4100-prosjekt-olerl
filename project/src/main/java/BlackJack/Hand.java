package BlackJack;

import java.util.ArrayList;
import java.util.List;

public class Hand {

	private List<Card> hand = new ArrayList<>();
	
	public int getSize() {
		return hand.size();
	}
	
	public Card getCard(int n) {
		return hand.get(n);
	}
	
	public int evaluateHand() {
		int value = 0;
		int amountAce = 0;
		for (int i=0; i<getSize(); i++) {    // legger sammen verdien til alle kort utenom ess
			if (getCard(i).getFace() > 9) {
				value += 10;
			} else if (getCard(i).getFace() == 1) {
				amountAce += 1;
			} else {
				value += getCard(i).getFace();
			}
		}
		if (amountAce > 0) {
			if (value + 11 + (amountAce - 1) < 22) {
				value += 11 + (amountAce - 1);
			} else {
				value += amountAce;
			}
		}
		return value;
	}
	
	public boolean notBust() {
		return evaluateHand() < 22;
	}
	
	public void addCardToHand(Card card) {
		if(hand.size()> 4) {
			throw new IllegalStateException("Player cannot have more than five cards in their hand.");
		}
		hand.add(card);
	}
	
	public void emptyHand() {
		int k = getSize();
		for(int i=0; i<k; i++)	{
			hand.remove(0);
		}
	}
	
	
}
