package BlackJack;

public class Dealer {
	
	private Hand hand;
	private BlackJack blackJack;
	
	public Dealer() {
		this.hand = new Hand();
	}
	
	public void setGame(BlackJack blackJack) {
		this.blackJack = blackJack;
	}
	
	public Hand getCurrentHand() {
		return hand;
	}
	
	public void addCardToHand(Card card) {
		hand.addCardToHand(card);
	}
	
	public void dealerPlay() {
		int handValue = hand.evaluateHand();
		if (handValue < 17 && hand.getSize() < 5) {
			addCardToHand(blackJack.deckPop());
			dealerPlay();
		}
	}
	
	public void emptyHand() {
		hand.emptyHand();
	}
	
}
