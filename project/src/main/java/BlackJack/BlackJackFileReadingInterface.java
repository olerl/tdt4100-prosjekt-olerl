package BlackJack;

import java.io.InputStream;
import java.io.OutputStream;

public interface BlackJackFileReadingInterface {
	
	BlackJack readBlackJack(InputStream is);
	
	BlackJack readBlackJack(String input, BlackJack blackJack);
	
	void writeBlackJack(BlackJack blackJack, OutputStream os);
	
	void writeBlackJack(BlackJack blackJack);
	
}
