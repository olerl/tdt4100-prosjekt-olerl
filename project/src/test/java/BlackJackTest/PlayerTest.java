package BlackJackTest;



import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import BlackJack.BlackJack;
import BlackJack.Player;

public class PlayerTest {

	private Player player;
	private BlackJack blackJack;
	
	@BeforeEach
	public void setup() {
		player = new Player();
		blackJack = new BlackJack();
		player.setGame(blackJack);
	}
	
	@Test
	public void constructorTest() {
		assertEquals(1000, player.getChipCount());
	}

	@Test
	public void placeBetTest() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {    
			player.placeBet("abc");
		});
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			player.placeBet("-100");
		});
		Assertions.assertThrows(IllegalStateException.class, () -> {    // sjekker at man ikke kan vedde mer en chip counten sin
			player.placeBet("1500");
		});
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			player.placeBet("0");
		});
		
		Assertions.assertDoesNotThrow(() -> {
			player.placeBet("300");
		});
		assertEquals(700, player.getChipCount());
		assertEquals(300, player.getCurrentBet());
	}
	
	@Test
	public void lostHandTest() {
		player.placeBet("100");
		player.lostHand();
		assertEquals(900, player.getChipCount());
		assertEquals(0, player.getCurrentBet());
		assertEquals("-100", blackJack.getPlayerHistory().get(0));
	}
	
	@Test
	public void wonHandTest() {
		player.placeBet("100");
		player.wonHand();
		assertEquals(1100, player.getChipCount());
		assertEquals(0, player.getCurrentBet());
		assertEquals("+100", blackJack.getPlayerHistory().get(0));
	}
	
	@Test
	public void tieHandTest() {
		player.placeBet("100");
		player.tieHand();
		assertEquals(1000, player.getChipCount());
		assertEquals(0, player.getCurrentBet());
		assertEquals("+0", blackJack.getPlayerHistory().get(0));
	}
	
}
