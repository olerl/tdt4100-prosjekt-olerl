package BlackJackTest;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import BlackJack.Card;
import BlackJack.Hand;

public class HandTest {

	private Hand hand;
	
	@BeforeEach
	public void setup() {
		hand = new Hand();
	}
	
	@Test
	public void addCardToHandTest() {
		hand.addCardToHand(new Card('S', 10));
		assertEquals(1, hand.getSize());
		hand.addCardToHand(new Card('S', 1));
		assertEquals(2, hand.getSize());
		hand.addCardToHand(new Card('S', 2));
		assertEquals(3, hand.getSize());
		hand.addCardToHand(new Card('S', 3));
		assertEquals(4, hand.getSize());
		Assertions.assertDoesNotThrow(() -> {    //sjekker at det ikke blir utl�st et unntak dersom vi legger til et femte kort
			hand.addCardToHand(new Card('S', 4));
		});
		assertEquals(5, hand.getSize());
		Assertions.assertThrows(IllegalStateException.class, () -> {    //sjekker at det blir utl�st et unntak dersom vi legger til et sjette kort
			hand.addCardToHand(new Card('S', 5));
		});
	}
	
	@Test
	public void emptyHandTest() {
		hand.addCardToHand(new Card('S', 10));
		hand.addCardToHand(new Card('S', 1));
		hand.addCardToHand(new Card('S', 2));
		hand.addCardToHand(new Card('S', 3));
		hand.emptyHand();
		assertEquals(0, hand.getSize());    //sjekker at h�nden blir t�mt etter kj�ring av emptyHand
	}
	
	@Test
	public void evaluateHandTest() {
		//sjekker at verdier blir lagt sammen riktig av evaluateHand i henhold til BlackJack regler
		hand.addCardToHand(new Card('S', 5));
		hand.addCardToHand(new Card('S', 6));
		assertEquals(11, hand.evaluateHand());
		hand.emptyHand();
		
		hand.addCardToHand(new Card('S', 4));
		hand.addCardToHand(new Card('S', 13));
		assertEquals(14, hand.evaluateHand());
		hand.emptyHand();
		
		hand.addCardToHand(new Card('S', 4));
		hand.addCardToHand(new Card('S', 3));
		hand.addCardToHand(new Card('S', 5));
		hand.addCardToHand(new Card('S', 6));
		assertEquals(18, hand.evaluateHand());
		hand.emptyHand();
		
		hand.addCardToHand(new Card('S', 12));
		hand.addCardToHand(new Card('S', 11));
		assertEquals(20, hand.evaluateHand());
		hand.emptyHand();
		
		hand.addCardToHand(new Card('S', 1));
		hand.addCardToHand(new Card('S', 6));
		assertEquals(17, hand.evaluateHand());
		hand.emptyHand();
		
		hand.addCardToHand(new Card('S', 1));
		hand.addCardToHand(new Card('S', 1));
		hand.addCardToHand(new Card('S', 10));
		assertEquals(12, hand.evaluateHand());
		hand.emptyHand();
		
		hand.addCardToHand(new Card('S', 1));
		hand.addCardToHand(new Card('S', 1));
		hand.addCardToHand(new Card('S', 1));
		hand.addCardToHand(new Card('S', 8));
		assertEquals(21, hand.evaluateHand());
		hand.emptyHand();
		
		hand.addCardToHand(new Card('S', 10));
		hand.addCardToHand(new Card('S', 13));
		hand.addCardToHand(new Card('S', 7));
		assertEquals(27, hand.evaluateHand());
		hand.emptyHand();
	}
	
	
	@Test
	public void notBustTest() {
		//sjekker at notBust returnerer riktig boolean i henhold til om spilleren har over 21 i verdi p� h�nden
		hand.addCardToHand(new Card('S', 1));
		hand.addCardToHand(new Card('S', 1));
		hand.addCardToHand(new Card('S', 1));
		hand.addCardToHand(new Card('S', 8));
		assertEquals(true, hand.notBust());
		hand.emptyHand();
		
		hand.addCardToHand(new Card('S', 12));
		hand.addCardToHand(new Card('S', 8));
		hand.addCardToHand(new Card('S', 6));
		assertEquals(false, hand.notBust());
		hand.emptyHand();
		
		hand.addCardToHand(new Card('S', 6));
		hand.addCardToHand(new Card('S', 9));
		hand.addCardToHand(new Card('S', 7));
		assertEquals(false, hand.notBust());
		hand.emptyHand();
	}

}
