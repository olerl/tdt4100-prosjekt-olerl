package BlackJackTest;



import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import BlackJack.BlackJack;
import BlackJack.Card;

// Tester ikke noen metoder ettersom de kun kaller p� metoder i andre klasser

public class BlackJackTest {
	
	private BlackJack game;

	@BeforeEach
	public void setup() {
		game = new BlackJack();
	}
	
	@Test
	public void deckTest() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			game.generateDeck(-1);
		});
		Assertions.assertDoesNotThrow(() -> {
			game.generateDeck(1);
		});
		assertEquals(52, game.getDeckSize());    //sjekker at riktig antall kort blir generert
		Assertions.assertDoesNotThrow(() -> {
			game.generateDeck(4);
		});
		assertEquals(208, game.getDeckSize());   //sjekker at dersom en ny deck blir generert s� blir tidligere kort fjernet og det er riktig antall
		game.shuffleDeck();
		assertEquals(208, game.getDeckSize());   //sjekker at shuffleDeck returnerer riktig antall kort
		Card card1 = new Card('S', 10);
		Card card2 = new Card('D', 3);
		Card card3 = new Card('C', 13);
		Card card4 = new Card('H', 7);
		int card1occurrence = 0;
		int card2occurrence = 0;
		int card3occurrence = 0;
		int card4occurrence = 0;
		
		for(int i=0; i<208; i++) {
			if(card1.getSuit() == game.getCard(i).getSuit() && card1.getFace() == game.getCard(i).getFace()) {
				card1occurrence ++;
			}
			if(card2.getSuit() == game.getCard(i).getSuit() && card2.getFace() == game.getCard(i).getFace()) {
				card2occurrence ++;
			}
			if(card3.getSuit() == game.getCard(i).getSuit() && card3.getFace() == game.getCard(i).getFace()) {
				card3occurrence ++;
			}
			if(card4.getSuit() == game.getCard(i).getSuit() && card4.getFace() == game.getCard(i).getFace()) {
				card4occurrence ++;
			}
		}
		assertEquals(4, card1occurrence);    // sjekker at et utvalg kort eksisterer i dekken riktig antall ganger etter shuffling
		assertEquals(4, card2occurrence);
		assertEquals(4, card3occurrence);
		assertEquals(4, card4occurrence);
	}
	
	@Test
	public void deckPopTest() {
		game.generateDeck(1);    //genererer kun 1 kortstokk slik at det ikke er noen duplikat kort
		Card card = game.getCard(0);
		game.deckPop();
		assertEquals(51, game.getDeckSize());
		Assertions.assertFalse(game.getCard(0).getSuit() == card.getSuit() && game.getCard(0).getFace() == card.getFace());
	}
	
	@Test
	public void dealHandsTest() {
		game.generateDeck(1);    //genererer kun en kortstokk for � enklere kunne sjekke om kortene er blitt fjernet
		game.dealHands();
		assertEquals(2, game.getPlayerHandSize());
		assertEquals(2, game.getDealerHandSize());
		Assertions.assertFalse(game.getDeck().contains(game.getPlayerHand().getCard(0)));    //sjekker at kortene er fjernet fra dekket
		Assertions.assertFalse(game.getDeck().contains(game.getPlayerHand().getCard(1)));
		Assertions.assertFalse(game.getDeck().contains(game.getDealerHand().getCard(0)));
		Assertions.assertFalse(game.getDeck().contains(game.getDealerHand().getCard(1)));
	}
	
	@Test
	public void handFinishedTest() {
		game.getPlayerHand().addCardToHand(new Card('S', 10));
		game.getPlayerHand().addCardToHand(new Card('S', 12));
		game.getPlayerHand().addCardToHand(new Card('S', 2));
		game.playerBet("100");
		game.getDealerHand().addCardToHand(new Card('H', 13));
		game.getDealerHand().addCardToHand(new Card('H', 11));
		game.getDealerHand().addCardToHand(new Card('H', 2));
		game.handFinished();
		assertEquals("900", game.getPlayerChipCount());
		assertEquals("0", game.getPlayerCurrentBet());     //sjekker at player tapte n�r b�de han og dealer bustet, og at currentBet er satt tilbake til 0
		game.getPlayerHand().emptyHand();
		game.getDealerHand().emptyHand();
		game.setPlayerChipCount(1000);
		
		game.getPlayerHand().addCardToHand(new Card('S', 11));
		game.getPlayerHand().addCardToHand(new Card('S', 12));
		game.playerBet("100");
		game.getDealerHand().addCardToHand(new Card('H', 13));
		game.getDealerHand().addCardToHand(new Card('H', 12));
		game.getDealerHand().addCardToHand(new Card('H', 2));
		game.handFinished();
		assertEquals("1100", game.getPlayerChipCount());
		assertEquals("0", game.getPlayerCurrentBet());     //sjekker at player vinner dersom han ikke buster og dealer buster, og at currentBet er satt tilbake til 0
		game.getPlayerHand().emptyHand();
		game.getDealerHand().emptyHand();
		game.setPlayerChipCount(1000);
		
		game.getPlayerHand().addCardToHand(new Card('S', 10));
		game.getPlayerHand().addCardToHand(new Card('S', 12));
		game.playerBet("100");
		game.getDealerHand().addCardToHand(new Card('H', 12));
		game.getDealerHand().addCardToHand(new Card('H', 7));
		game.handFinished();
		assertEquals("1100", game.getPlayerChipCount());
		assertEquals("0", game.getPlayerCurrentBet());     //sjekker at player vinner dersom ingen buster, men han har h�yere verdi
		game.getPlayerHand().emptyHand();
		game.getDealerHand().emptyHand();
		game.setPlayerChipCount(1000);
		
		game.getPlayerHand().addCardToHand(new Card('S', 11));
		game.getPlayerHand().addCardToHand(new Card('S', 13));
		game.playerBet("100");
		game.getDealerHand().addCardToHand(new Card('H', 12));
		game.getDealerHand().addCardToHand(new Card('H', 10));
		game.handFinished();
		assertEquals("1000", game.getPlayerChipCount());
		assertEquals("0", game.getPlayerCurrentBet());    //sjekker at det blir likt dersom de har lik verdi og ingen buster
		game.getPlayerHand().emptyHand();
		game.getDealerHand().emptyHand();
		game.setPlayerChipCount(1000);
		
		game.getPlayerHand().addCardToHand(new Card('S', 10));
		game.getPlayerHand().addCardToHand(new Card('S', 6));
		game.playerBet("100");
		game.getDealerHand().addCardToHand(new Card('H', 12));
		game.getDealerHand().addCardToHand(new Card('H', 7));
		game.handFinished();
		assertEquals("900", game.getPlayerChipCount());
		assertEquals("0", game.getPlayerCurrentBet());    //sjekker at player taper dersom han har lavere verdi og ingen buster
		game.getPlayerHand().emptyHand();
		game.getDealerHand().emptyHand();
		game.setPlayerChipCount(1000);
		
		game.getPlayerHand().addCardToHand(new Card('S', 10));
		game.getPlayerHand().addCardToHand(new Card('S', 6));
		game.getPlayerHand().addCardToHand(new Card('S', 8));
		game.playerBet("100");
		game.getDealerHand().addCardToHand(new Card('H', 12));
		game.getDealerHand().addCardToHand(new Card('H', 7));
		game.handFinished();
		assertEquals("900", game.getPlayerChipCount());
		assertEquals("0", game.getPlayerCurrentBet());    //sjekker at player taper dersom han buster og dealer buster ikke
		game.getPlayerHand().emptyHand();
		game.getDealerHand().emptyHand();
		game.setPlayerChipCount(1000);
	}
	
	
	@Test
	public void getDeckTest() {
		game.getDeck().remove(0);
		assertEquals(208, game.getDeckSize());    //sjekker at man ikke kan endre p� listen ved � bruke getDeck
	}
	
	@Test
	public void gameOverTest() {
		game.setPlayerChipCount(0);
		assertEquals(true, game.gameOver());
		assertEquals("You are out of chips. Better luck next time!", game.getPlayerMessage());    //sjekker at n�r spiller har 0 chips returnerer game over true og at playerMessage blir satt riktig
		
		game.setPlayerChipCount(100);
		assertEquals(false, game.gameOver());    //sjekker at n�r spiller har chips igjen returnerer gameOver false
	}
	
	@Test
	public void getPlayerHistoryTest() {
		game.addToPlayerHistory("+100");
		game.getPlayerHistory().remove(0);
		assertEquals("+100", game.getPlayerHistory().get(0));    //sjekker at man ikke kan endre p� listen ved � bruke getPlayerHistory
	}

}
