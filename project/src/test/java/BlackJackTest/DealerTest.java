package BlackJackTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import BlackJack.BlackJack;
import BlackJack.Card;
import BlackJack.Dealer;

class DealerTest {

	// velger � ikke teste emptyHand og addCardToHand ettersom de blir sendt videre til hand klassen og vi tester de der
	
	private Dealer dealer;
	private BlackJack blackJack;
	
	@BeforeEach
	public void setup() {
		dealer = new Dealer();
		blackJack = new BlackJack();
		dealer.setGame(blackJack);
	}
	
	@Test
	public void testDealerPlay() {
		dealer.addCardToHand(new Card('S', 10));
		dealer.addCardToHand(new Card('S', 7));
		dealer.dealerPlay();
		assertEquals(2, dealer.getCurrentHand().getSize());    //sjekker at dealer ikke "hitter" n�r verdi er 17
		dealer.emptyHand();
		
		dealer.addCardToHand(new Card('S', 2));      //sjekker at dealer ikke "hitter" n�r den har fem kort
		dealer.addCardToHand(new Card('H', 2));
		dealer.addCardToHand(new Card('D', 2));
		dealer.addCardToHand(new Card('C', 2));
		dealer.addCardToHand(new Card('S', 3));
		dealer.dealerPlay();
		assertEquals(5, dealer.getCurrentHand().getSize());
		dealer.emptyHand();
		
		dealer.addCardToHand(new Card('S', 10));
		dealer.addCardToHand(new Card('S', 6));
		dealer.dealerPlay();
		assertEquals(3, dealer.getCurrentHand().getSize());      //sjekker at dealer "hitter" n�r verdi er 16	
	}

}
